package com.warion.autohub;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.warion.autohub.activity.MainObdActivity;

import java.util.ArrayList;

/**
 * Created by user on 2/27/2018.
 */

class AutoAdapter extends RecyclerView.Adapter<AutoAdapter.AutoViewHolder> {
    private GradientDrawable mGradientDrawable;
    private ArrayList<Auto> mAutoData;
    private Context mContext;




    AutoAdapter(Context context, ArrayList<Auto> autoData) {
        this.mAutoData = autoData;
        this.mContext = context;

        //Prepare gray placeholder
        mGradientDrawable = new GradientDrawable();
        mGradientDrawable.setColor(Color.GRAY);

        //Make the placeholder same size as the images
        Drawable drawable = ContextCompat.getDrawable
                (mContext, R.drawable.chat);
        if (drawable != null) {
            mGradientDrawable.setSize(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
    }


    @Override
    public AutoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AutoViewHolder(mContext, LayoutInflater.from(mContext).
                inflate(R.layout.list_item, parent, false), mGradientDrawable);
    }

    @Override
    public void onBindViewHolder(AutoViewHolder holder, int position) {
        //Get the current sport
        Auto currentAuto = mAutoData.get(position);

        //Bind the data to the views
        holder.bindTo(currentAuto);
    }

    @Override
    public int getItemCount() {
        return mAutoData.size();
    }

    static class AutoViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        //Member Variables for the holder data
        private TextView mTitleText;
        private TextView mInfoText;
        private ImageView mAutoImage;
        private Context mContext;
        private Auto mCurrentAuto;
        private GradientDrawable mGradientDrawable;

        /**
         * Constructor for the SportsViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the list_item.xml layout file
         */
        AutoViewHolder(Context context, View itemView, GradientDrawable gradientDrawable) {
            super(itemView);

            //Initialize the views
            mTitleText = (TextView) itemView.findViewById(R.id.title);
            mInfoText = (TextView) itemView.findViewById(R.id.subTitle);
            mAutoImage = (ImageView) itemView.findViewById(R.id.autoImage);

            mContext = context;
            mGradientDrawable = gradientDrawable;

            //Set the OnClickListener to the whole view
            itemView.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View view) {
//            Intent detailIntent = Auto.starter(mContext, mCurrentAuto.getTitle(),
//                    mCurrentAuto.getImageResource());
//
//
//            //Start the detail activity
//            mContext.startActivity(detailIntent);
//        }

        public void bindTo(Auto currentAuto) {
            mTitleText.setText(currentAuto.getTitle());
            mInfoText.setText(currentAuto.getInfo());

            //Get the current sport
            mCurrentAuto = currentAuto;



            //Load the images into the ImageView using the Glide library
            Glide.with(mContext).load(currentAuto.
                    getImageResource()).placeholder(mGradientDrawable).into(mAutoImage);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();


            final Intent intent;
            switch (getAdapterPosition()) {

                case 0:
                    intent = new Intent(mContext, ForumActivity.class);
                    break;
                case 1:
                    intent = new Intent(mContext, CarMaps.class);
                    break;
                case 2:
                    intent = new Intent(mContext, FuelActivity.class);
                    break;
                case 3:
                    intent = new Intent(mContext, MainObdActivity.class);
                    break;
                case 4:
                    intent = new Intent(mContext, ProfileActivity.class);
                    break;

                default:
                    intent = new Intent(mContext, MainActivity.class);
                    break;

            }

            mContext.startActivity(intent);
        }
    }
}
