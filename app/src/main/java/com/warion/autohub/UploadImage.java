package com.warion.autohub;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class UploadImage extends AppCompatActivity implements View.OnClickListener {
    private static final int RESULT_LOAD_IMAGE = 1;

    private Button buttonChoose, buttonUpload;
    private ImageView hasil;
    private TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);

        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);
        hasil = (ImageView) findViewById(R.id.hasil);
        name = (TextView) findViewById(R.id.name);

        buttonChoose.setOnClickListener(this);
        buttonUpload.setOnClickListener((this));
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonChoose:
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD_IMAGE);

            break;
            case R.id.buttonUpload:

                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE &&resultCode == RESULT_OK && data != null){
            Uri selectedImage = data.getData();
            hasil.setImageURI(selectedImage);
        }
    }
}