package com.warion.autohub.config;

import com.warion.autohub.commands.ObdCommand;
import com.warion.autohub.commands.SpeedCommand;
import com.warion.autohub.commands.control.DistanceMILOnCommand;
import com.warion.autohub.commands.control.DtcNumberCommand;
import com.warion.autohub.commands.control.EquivalentRatioCommand;
import com.warion.autohub.commands.control.ModuleVoltageCommand;
import com.warion.autohub.commands.control.TimingAdvanceCommand;
import com.warion.autohub.commands.control.TroubleCodesCommand;
import com.warion.autohub.commands.control.VinCommand;
import com.warion.autohub.commands.engine.LoadCommand;
import com.warion.autohub.commands.engine.MassAirFlowCommand;
import com.warion.autohub.commands.engine.OilTempCommand;
import com.warion.autohub.commands.engine.RPMCommand;
import com.warion.autohub.commands.engine.RuntimeCommand;
import com.warion.autohub.commands.engine.ThrottlePositionCommand;
import com.warion.autohub.commands.fuel.AirFuelRatioCommand;
import com.warion.autohub.commands.fuel.ConsumptionRateCommand;
import com.warion.autohub.commands.fuel.FindFuelTypeCommand;
import com.warion.autohub.commands.fuel.FuelLevelCommand;
import com.warion.autohub.commands.fuel.FuelTrimCommand;
import com.warion.autohub.commands.fuel.WidebandAirFuelRatioCommand;
import com.warion.autohub.commands.pressure.BarometricPressureCommand;
import com.warion.autohub.commands.pressure.FuelPressureCommand;
import com.warion.autohub.commands.pressure.FuelRailPressureCommand;
import com.warion.autohub.commands.pressure.IntakeManifoldPressureCommand;
import com.warion.autohub.commands.temperature.AirIntakeTemperatureCommand;
import com.warion.autohub.commands.temperature.AmbientAirTemperatureCommand;
import com.warion.autohub.commands.temperature.EngineCoolantTemperatureCommand;
import com.warion.autohub.enums.FuelTrim;

import java.util.ArrayList;

/**
 * TODO put description
 */
public final class ObdConfig {

    public static ArrayList<ObdCommand> getCommands() {
        ArrayList<ObdCommand> cmds = new ArrayList<>();

        // Control
        cmds.add(new ModuleVoltageCommand());
        cmds.add(new EquivalentRatioCommand());
        cmds.add(new DistanceMILOnCommand());
        cmds.add(new DtcNumberCommand());
        cmds.add(new TimingAdvanceCommand());
        cmds.add(new TroubleCodesCommand());
        cmds.add(new VinCommand());

        // Engine
        cmds.add(new LoadCommand());
        cmds.add(new RPMCommand());
        cmds.add(new RuntimeCommand());
        cmds.add(new MassAirFlowCommand());
        cmds.add(new ThrottlePositionCommand());

        // Fuel
        cmds.add(new FindFuelTypeCommand());
        cmds.add(new ConsumptionRateCommand());
        // cmds.add(new AverageFuelEconomyObdCommand());
        //cmds.add(new FuelEconomyCommand());
        cmds.add(new FuelLevelCommand());
        // cmds.add(new FuelEconomyMAPObdCommand());
        // cmds.add(new FuelEconomyCommandedMAPObdCommand());
        cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_1));
        cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_2));
        cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_1));
        cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_2));
        cmds.add(new AirFuelRatioCommand());
        cmds.add(new WidebandAirFuelRatioCommand());
        cmds.add(new OilTempCommand());

        // Pressure
        cmds.add(new BarometricPressureCommand());
        cmds.add(new FuelPressureCommand());
        cmds.add(new FuelRailPressureCommand());
        cmds.add(new IntakeManifoldPressureCommand());

        // Temperature
        cmds.add(new AirIntakeTemperatureCommand());
        cmds.add(new AmbientAirTemperatureCommand());
        cmds.add(new EngineCoolantTemperatureCommand());

        // Misc
        cmds.add(new SpeedCommand());


        return cmds;
    }

}
