package com.warion.autohub.io;

public interface ObdProgressListener {

    void stateUpdate(final ObdCommandJob job);

}